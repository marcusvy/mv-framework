# Marcus Vinícius Framework

*A PHP Framework for projects solutions and fast products using PHP best practices.*

MV Framework is a professional and open source tool for creating digital solutions, which combines speed, security and solidity of a project, following PHP best practices.

## Focus
- Security (follow OWASP specifications)
- Clean Arquitecture
- User Experience
- DevOps
- Fast
- Simplicity
- Best Practices
