<?php

declare(strict_types=1);

/**
 * Set a enviroment value
 *
 * @param string $name Name of enviroment variable
 * @param mixed $value The value of the variable
 */
function mv_env_set(string $name, mixed $value): void
{
    $_ENV[$name] = $value;
}

/**
 * Get a enviroment value by his name
 *
 * @param string $name Name of enviroment variable
 */
function mv_env_get(string $name): mixed
{
    return $_ENV[$name];
}
