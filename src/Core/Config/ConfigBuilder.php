<?php

declare(strict_types=1);

namespace Marcus\Core\Config;

class ConfigBuilder
{
    public function build(): Config
    {
        $config = new Config();
        $config->load();
        return $config;
    }
}
