<?php

declare(strict_types=1);

namespace Marcus\Core\Config;

use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use RecursiveRegexIterator;
use RegexIterator;

class Config
{
  public array $env = [];
  public array $routes = [];
  public array $dev = [];
  public array $prod = [];
  public array $config = [];

  /**
   * @psalm-suppress UnresolvableInclude
   */
  public function load(): void
  {
    $projectPath = dirname(getcwd());
    $configFolder = sprintf("%s/config/", $projectPath);

    $phpConfigFiles = $this->readConfigFolder($configFolder);
    /** @var string|array<int, string> $configFile */
    foreach ($phpConfigFiles as $configFile) {
      if (is_array($configFile)) {
        $configFile = $configFile[0];
      }
      $basename = basename($configFile, '.php');
      $dirname = basename(dirname($configFile));
      $filesToInclude = ['config', 'env'];
      switch ($dirname) {
        case 'config':
          if (in_array($basename, $filesToInclude)) {
            if ($basename === 'env') {
              include $configFile;
              $this->env = array_merge_recursive($this->env, $_ENV);
            }
          }
          break;
        case 'autoload':
          /** @var array $load */
          $load = include $configFile;
          $this->config = array_merge_recursive($this->config, $load);
          break;
        case 'dev':
          /** @var array $load */
          $load = include $configFile;
          $this->dev = array_merge_recursive($this->dev, $load);
          break;
        case 'prod':
          /** @var array $load */
          $load = include $configFile;
          $this->prod = array_merge_recursive($this->prod, $load);
          break;
      }
    }
  }


  /**
   * Undocumented function
   *
   * @param string $configFolder
   * @return RegexIterator
   */
  private function readConfigFolder(string $configFolder): RegexIterator
  {
    $dir = new RecursiveDirectoryIterator($configFolder);
    $iterator = new RecursiveIteratorIterator($dir);
    $phpConfigFiles = new RegexIterator($iterator, '/^.+\.php$/i', RecursiveRegexIterator::GET_MATCH);
    return $phpConfigFiles;
  }
}
