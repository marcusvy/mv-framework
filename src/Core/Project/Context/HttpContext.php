<?php

declare(strict_types=1);

namespace Marcus\Core\Project\Context;

use Psr\Container\ContainerInterface;

class HttpContext implements ContextInterface
{
    private ?ContainerInterface $container = null;

    public function getContainer(): ?ContainerInterface
    {
        return $this->container;
    }

    public function setContainer(?ContainerInterface $container): void
    {
        $this->container = $container;
    }
}
