<?php

declare(strict_types=1);

namespace Marcus\Core\Server\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

use function file_exists;
use function getcwd;
use function sprintf;
use function unlink;

use const PHP_EOL;

class ClearConfigCacheCommand extends Command
{
    public const OUTPUT_CONFIG_FILE_NOT_FOUND       = "No configuration file found.%s";
    public const OUTPUT_CONFIG_NOT_FOUND            = "Parameter 'config_cache_path' not found on config.%s";
    public const OUTPUT_CONFIG_CACHE_FILE_NOT_FOUND = "Configured config cache file '%s' not found%s";
    public const OUTPUT_ERROR_ON_DELETE             = "Error removing config cache file '%s'%s";
    public const OUTPUT_SUCCESS                     = "Removed configured config cache file '%s'%s";

    /** @var string|null */
    protected static $defaultName = 'clear:config:cache';

  /**
   * @psalm-suppress ForbiddenCode
   */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io         = new SymfonyStyle($input, $output);
        $configFile = getcwd() . '/config/config.php';
        if (! file_exists($configFile)) {
            $io->warning(sprintf(self::OUTPUT_CONFIG_FILE_NOT_FOUND, PHP_EOL));
            return Command::FAILURE;
        }
      /** @var array $config */
        $config = include $configFile;
        return $this->clearConfigCacheFromConfig($config, $io);
    }

  /**
   * @psalm-suppress ForbiddenCode
   */
    private function clearConfigCacheFromConfig(array $config, SymfonyStyle $io): int
    {
      /** @var string $configCachePath */
        $configCachePath = $config['config_cache_path'];

        if (! isset($config['config_cache_path'])) {
            $mesage = sprintf(self::OUTPUT_CONFIG_NOT_FOUND, PHP_EOL);
            $io->warning($mesage);
            return Command::FAILURE;
        }

        if (! file_exists($configCachePath)) {
            $mesage = sprintf(
                self::OUTPUT_CONFIG_CACHE_FILE_NOT_FOUND,
                $configCachePath,
                PHP_EOL
            );
            $io->warning($mesage);
            return Command::SUCCESS;
        }

        if (false === unlink($configCachePath)) {
            $mesage = sprintf(
                self::OUTPUT_ERROR_ON_DELETE,
                $configCachePath,
                PHP_EOL
            );
            $io->warning($mesage);
            return Command::FAILURE;
        }

        $mesage = sprintf(
            self::OUTPUT_SUCCESS,
            $configCachePath,
            PHP_EOL
        );
        $io->warning($mesage);
        return Command::SUCCESS;
    }
}
