<?php

declare(strict_types=1);

namespace Marcus\Core\Server\Command;

use Composer\Script\Event;

class Composer
{
    public static function execute(Event $event): void
    {
        $message = "MV Framework";
        $io      = $event->getIO();
        // $io->info($message);
        $io->write($message);
        // $io->debug($message);
    }
}
