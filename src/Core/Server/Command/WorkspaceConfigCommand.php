<?php

declare(strict_types=1);

namespace Marcus\Core\Server\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

use function copy;
use function getcwd;
use function is_bool;
use function realpath;
use function sprintf;

class WorkspaceConfigCommand extends Command
{
    /** @var string|null */
    protected static $defaultName = 'workspace:config';

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        return $this->clearWrokspace($io);
    }

    private function clearWrokspace(SymfonyStyle $io): int
    {
        $developerFiles = [
            'phpcs.xml',
            'phpunit.xml',
            'psalm.xml',
        ];

        foreach ($developerFiles as $file) {
            $distFile = realpath(getcwd() . "/$file.dist");

            if (is_bool($distFile)) {
                $io->warning(sprintf("The file '%s' was not found", $file));
            }

            $configFile = realpath(getcwd() . "/$file");
            if (is_bool($configFile)) {
                $io->info(sprintf("File -> '%s' will be created.", $file));
                $configFile = getcwd() . "/$file";
                copy($distFile, $configFile);
                $io->info(sprintf("File -> '%s' created.", $file));
            } else {
                $io->info(sprintf("File -> '%s' already exist.", $file));
            }
        }

        return Command::SUCCESS;
    }
}
