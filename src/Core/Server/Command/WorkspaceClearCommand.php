<?php

declare(strict_types=1);

namespace Marcus\Core\Server\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

use function getcwd;
use function is_bool;
use function realpath;
use function sprintf;
use function unlink;

class WorkspaceClearCommand extends Command
{
    /** @var string|null */
    protected static $defaultName = 'workspace:clear';

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        return $this->clearWrokspace($io);
    }

    private function clearWrokspace(SymfonyStyle $io): int
    {
        $developerFiles = [
            '.phpcs-cache',
            '.phpunit.result.cache',
            'phpcs.xml',
            'phpunit.xml',
            'psalm.xml',
        ];

        foreach ($developerFiles as $file) {
            $filepath = realpath(getcwd() . "/$file");

            if (is_bool($filepath)) {
                $io->info(sprintf("File '%s' is clean.", $file));
            } else {
                if (unlink($filepath)) {
                    $io->info(sprintf("Clean -> %s", $file));
                } else {
                    $io->info(sprintf("The file '%s' was not deleted", $file));
                }
            }
        }

        return Command::SUCCESS;
    }
}
