<?php

declare(strict_types=1);

namespace Marcus\Core\Server\Router;

interface RouterInterface
{
  /**
   * Resolve the router from URL
   */
    public function resolve(): void;
}
