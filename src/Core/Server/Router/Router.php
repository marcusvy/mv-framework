<?php

declare(strict_types=1);

namespace Marcus\Core\Server\Router;

use Psr\Container\ContainerInterface;

class Router implements RouterInterface
{
    private ?ContainerInterface $container;
    private array $config = [];

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function resolve(): void
    {
        if ($this->container instanceof ContainerInterface) {
            /** @var array $config */
            $config       = $this->container->has('config') ? $this->container->get('config') : [];
            $this->config = $config;
        }
    }
}
