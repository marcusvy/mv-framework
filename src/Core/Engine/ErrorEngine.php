<?php

declare(strict_types=1);

namespace Marcus\Core\Engine;

use Psr\Container\ContainerInterface;
use Whoops\Handler\HandlerInterface;
use Whoops\Handler\PrettyPageHandler;
use Whoops\Run;
use Whoops\RunInterface;

use const PHP_EOL;

class ErrorEngine implements EngineAwareInterface
{
    private ?Run $instance = null;

    public function getInstance(): ?RunInterface
    {
        return $this->instance;
    }

    public function init(?ContainerInterface $container): void
    {
        if ($container !== null) {
            $this->instance = new Run();
            /** @var HandlerInterface $handler */
            $handler = $container->get(PrettyPageHandler::class);
            $this->instance->pushHandler($handler);
            $this->instance->register();
        }
    }

    public function process(): void
    {
        echo "Error" . PHP_EOL;
    }
}
