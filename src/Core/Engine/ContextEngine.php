<?php

declare(strict_types=1);

namespace Marcus\Core\Engine;

use Marcus\Core\Project\Context\ContextInterface;
use Marcus\Core\Project\Context\HttpContext;
use Psr\Container\ContainerInterface;

use const PHP_EOL;

class ContextEngine implements EngineAwareInterface
{
    private ?ContextInterface $instance = null;

    public function getInstance(): ?ContextInterface
    {
        return $this->instance;
    }

    public function init(?ContainerInterface $container): void
    {
        if ($container instanceof ContainerInterface) {
            /** @var HttpContext $i */
            $i              = $container->get(HttpContext::class);
            $this->instance = $i;
            $this->instance->setContainer($container);
        }
    }

    public function process(): void
    {
        echo "Context" . PHP_EOL;
    }
}
