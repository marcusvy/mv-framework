<?php

declare(strict_types=1);

namespace Marcus\Core\Engine;

/**
 * @template T
 */
interface EngineInterface
{
    public function init(): void;

    public function process(): void;

    /**
     * @return T
     */
    public function getInstance();
}
