<?php

declare(strict_types=1);

namespace Marcus\Core\Engine;

use DI\Container;

/**
 * @template T
 */
interface EngineAwareInterface
{
    /**
     * @return T
     */
    public function getInstance();

    public function init(?Container $container): void;

    public function process(): void;
}
