<?php

declare(strict_types=1);

namespace Marcus\Core\Engine;

use DI\Container;
use DI\ContainerBuilder;

class ContainerEngine implements EngineInterface
{
    private ?Container $instance = null;

    public function getInstance(): ?Container
    {
        return $this->instance;
    }

    public function init(): void
    {
        $builder        = new ContainerBuilder();
        $this->instance = $builder->build();
    }

    public function process(): void
    {
        if ($this->instance !== null) {
            /** @var Container $instance */
            $instance = $this->getInstance();
            $instance->set('config', []);
        }
    }
}
