<?php

declare(strict_types=1);

namespace Marcus\Core\Engine;

use Marcus\Core\Server\Router\Router;
use Marcus\Core\Server\Router\RouterInterface;
use Psr\Container\ContainerInterface;

use const PHP_EOL;

class RouterEngine implements EngineAwareInterface
{
    private ?RouterInterface $instance = null;

    public function getInstance(): ?RouterInterface
    {
        return $this->instance;
    }

    public function init(?ContainerInterface $container): void
    {
        if ($container !== null) {
            /** @var RouterInterface $router */
            $router         = $container->get(Router::class);
            $this->instance = $router;
        }
    }

    public function process(): void
    {
        echo "Router" . PHP_EOL;
    }
}
