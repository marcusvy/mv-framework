<?php

declare(strict_types=1);

namespace Marcus\Core\Engine;

use DI\Container;
use Marcus\Core\Config\Config;
use Marcus\Core\Config\ConfigBuilder;

use const PHP_EOL;

class ConfigEngine implements EngineAwareInterface
{
    private ?Config $instance = null;

    public function getInstance(): ?Config
    {
        return $this->instance;
    }

    public function init(?Container $container): void
    {
        $configBuilder  = new ConfigBuilder();
        $this->instance = $configBuilder->build();
        if ($container instanceof Container) {
            $container->set(Config::class, $this->instance);
        }
    }

    public function process(): void
    {
        echo "Config" . PHP_EOL;
    }
}
