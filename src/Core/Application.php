<?php

declare(strict_types=1);

namespace Marcus\Core;

use Exception;
use Marcus\Core\Config\Config;
use Marcus\Core\Engine\ConfigEngine;
use Marcus\Core\Engine\ContainerEngine;
use Marcus\Core\Engine\ContextEngine;
use Marcus\Core\Engine\EngineAwareInterface;
use Marcus\Core\Engine\EngineInterface;
use Marcus\Core\Engine\ErrorEngine;
use Marcus\Core\Engine\RouterEngine;
use Marcus\Core\Project\Context\ContextInterface;
use Marcus\Core\Server\Router\RouterInterface;
use Psr\Container\ContainerInterface;
use SplObjectStorage;
use Whoops\RunInterface;

use function class_exists;
use function implode;
use function in_array;
use function spl_object_hash;
use function sprintf;

class Application
{
    public const MODE_DEFAULT = 'default';

    private SplObjectStorage $engines;
    private array $enginesHash  = [];
    private array $coreEngines  = [
        'container' => ContainerEngine::class,
    ];
    private array $awareEngines = [
        'config'  => ConfigEngine::class,
        'error'   => ErrorEngine::class,
        'context' => ContextEngine::class,
        'router'  => RouterEngine::class,
    ];

    private ?Config $config                = null;
    private ?ContainerInterface $container = null;
    private ?RunInterface $error           = null;
    private ?ContextInterface $context     = null;
    private ?RouterInterface $router       = null;
    private string $mode                   = self::MODE_DEFAULT;

    public function __construct(string $mode = self::MODE_DEFAULT)
    {
        $this->engines = new SplObjectStorage();
        $this->initDefaultEngines();
        $this->checkMode($mode);
    }

    public function init(): void
    {
        if ($this->mode === self::MODE_DEFAULT) {
            /** Start all engines */
            $this->initEngines();
        }
    }

    public function run(): void
    {
        /** @var EngineInterface $engine */
        foreach ($this->engines as $engine) {
            $engine->process();
        }
    }

    public function destroy(): void
    {
    }

    public function getContainer(): ?ContainerInterface
    {
        if ($this->container !== null) {
            $this->initContainerEngine();
        }
        return $this->container;
    }

    public function getError(): ?RunInterface
    {
        return $this->error;
    }

    public function getContext(): ?ContextInterface
    {
        return $this->context;
    }

    private function checkMode(string $mode): void
    {
        $availableModes = [self::MODE_DEFAULT];

        if (! in_array($mode, $availableModes, true)) {
            $message = "Application mode not found. Available modes: %s.";
            $message = sprintf($message, implode(", ", $availableModes));
            throw new Exception($message, 1000);
        }
        $this->mode = $mode;
    }

    private function initDefaultEngines(): void
    {
        $this->initContainerEngine();
        $this->initConfigEngine();
        $this->initErrorEngine();
    }

    /**
     * @psalm-suppress UndefinedClass
     */
    private function initEngines(): void
    {
        /** @var string $engine */
        foreach ($this->coreEngines as $name => $engine) {
            /** @var ?EngineInterface $e */
            $e = $this->initEngine($engine);
            if ($e !== null) {
                $this->$name = $e->getInstance();
            }
        }

        /** @var string $engine */
        foreach ($this->awareEngines as $name => $engine) {
            /** @var ?EngineAwareInterface $e */
            $e = $this->initEngine($engine, $this->container);
            if ($e !== null) {
                $this->$name = $e->getInstance();
            }
        }
    }

    /**
     * @psalm-suppress MixedMethodCall
     */
    private function initEngine(
        string $engine,
        ?ContainerInterface $container = null
    ): EngineInterface|EngineAwareInterface|null {
        if (class_exists($engine)) {
            if (! in_array($engine, $this->enginesHash)) {
                /** @var EngineInterface|EngineAwareInterface $e */
                $e = new $engine();
                if ($container === null) {
                    $e->init();
                } else {
                    $e->init($this->container);
                }
                // if ($propertyName !== null) {
                //     $instance = $e->getInstance();
                //     $this->$propertyName = $instance;
                // }
                $this->engines->attach($e);
                $this->enginesHash[spl_object_hash($e)] = $engine;
                return $e;
            }
        }

        return null;
    }

    private function initContainerEngine(): void
    {
        if (! in_array(ContainerEngine::class, $this->enginesHash)) {
            $engine = new ContainerEngine();
            $engine->init();
            /** @var ContainerInterface $instance */
            $instance        = $engine->getInstance();
            $this->container = $instance;
            $this->engines->attach($engine);
            $this->enginesHash[spl_object_hash($engine)] = ContainerEngine::class;
        }
    }

    private function initErrorEngine(): void
    {
        if (! in_array(ErrorEngine::class, $this->enginesHash)) {
            $engine = new ErrorEngine();
            $engine->init($this->container);
            /** @var RunInterface $instance */
            $instance    = $engine->getInstance();
            $this->error = $instance;
            $this->engines->attach($engine);
            $this->enginesHash[spl_object_hash($engine)] = ErrorEngine::class;
        }
    }

    private function initConfigEngine(): void
    {
        if (! in_array(ConfigEngine::class, $this->enginesHash)) {
            $engine = new ConfigEngine();
            $engine->init($this->container);
            /** @var Config $instance */
            $instance     = $engine->getInstance();
            $this->config = $instance;
            $this->engines->attach($engine);
            $this->enginesHash[spl_object_hash($engine)] = ConfigEngine::class;
        }
    }

    // private function initContextEngine(): void
    // {
    //     if (!in_array(ContextEngine::class, $this->enginesHash)) {
    //         $engine = new ContextEngine();
    //         $engine->init($this->container);
    //         /** @var ContextInterface $instance */
    //         $instance      = $engine->getInstance();
    //         $this->context = $instance;
    //         $this->engines->attach($engine);
    //         $this->enginesHash[spl_object_hash($engine)] = ContextEngine::class;
    //     }
    // }

    // private function initRouterEngine(): void
    // {
    //     if (!in_array(RouterEngine::class, $this->enginesHash)) {
    //         $engine = new RouterEngine();
    //         $engine->init($this->container);
    //         /** @var RouterInterface $instance */
    //         $instance     = $engine->getInstance();
    //         $this->router = $instance;
    //         $this->engines->attach($engine);
    //         $this->enginesHash[spl_object_hash($engine)] = RouterEngine::class;
    //     }
    // }
}
