<?php
declare(strict_types=1);

return [
  'config_cache_path' => __DIR__."/../data/cache/config-cache.php"
];
