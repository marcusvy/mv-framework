# Security

## Strategies

Many strategies has been adopted to make MV Framework safe. Check the main reasons that differ from other frameworks.

### Dot Env File

The '.env' file is only used for deployment infrastructure. This file may expose sensitive information that could help a malicious user to prepare more advanced attacks. It's recommended to remove or restrict access to this type of files from production systems.

MV uses the "/config/env.php" file, because, the environment variables will only exist for the duration of the current request. At the end of the request the environment is restored to its original state.

Be sure to remove or restrict access to all configuration files acessible from internet.
