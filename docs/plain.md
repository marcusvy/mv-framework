# Planejamento

## Introdução

Ao longo dos anos a facilidade de uso dos frameworks PHP vêm crescendo consideravelmente e gerando ótimas soluções para o mercado.
Entretanto, a facilidade traz um custo muito alto para o desempenho do sistema e a agilidade de produção para problemas mais complexos.
Vale frisar que as metodologias de desenvolvimento de software, em uma visão ampla, mudam constantemente se tornando mais uma "moda" do que uma solução científica de longo prazo.

Outro fator, é a tendência de solucionar problemas com PHP, utilizando uma mentalidade de outra linguagem de programação. Ao invés de utilizar as soluções nativas do PHP, onde na maioria das vezes possuem maior desempenho e praticidade, investem tempo em adaptar soluções externas que não obedecem ao propósito da linguagem PHP, gerando códigos sujos, difíceis de se manter e extremantente complexos.

O MV Framework é uma ferramenta profissional para a criação de soluções digitais, que une a rapidez, segurança e solidez de um projeto, obedecendo as melhores práticas PHP.

## Princípios

- Divisão do BackEnd do FrontEnd
    - BackEnd: Aplicação Servidora contendo regras de negócios e segurança
    - FronEnd: Aplicação Cliente contendo camada de visualização e interação do usuário com BackEnd
- Um documento HTML deve sempre ser gerado por uma camada de visualização específica e separada
- Qualquer parâmetro de uma URL deve vir de uma QueryString
- Cada URL é um handler único
- Cada manipulador tem sua própria Classe Handler e Classe Factory
- Cada URL é direcionado para um pipeline que processará middlewares
- Toda entrada de informação deve ser filtrada antes de ser processada

## Getting Started
- Setup / Installation
- Creating Pages
- Routing / Generating URLs
- Handlers
- Templates
- Configuration / Env Vars

## Front-end
- UX - Tailwindcss/Typescript
- Webpack Encore
- React.js
- Vue.js
- Bootstrap
- Web Assets
- WebLink

## Architecture
- Requests / Responses
- Kernel
- Services / DI
- Events
- Interfaces (Contracts)
- Bundles

## Introduction
- Users
- Authentication / Firewalls
- Authorization / Voters
- Passwords
- CSRF
- LDAP


## The Basics
- Databases / Doctrine
- Forms
- Tests
- Sessions
- Cache
- Logger
- Errors / Debugging

## Advanced Topics
- Console
- Mailer / Emails
- Validation
- Messaging / Queues
- Notifications
- Serialization
- Translation / i18n
-

## Utilities
- HTTP Client
- Files / Filesystem
- Expression Language
- Locks
- Workflows
- Strings / Unicode
- UID / UUID
- YAML Parser

## Production
- Deployment
- Performance
- HTTP Cache
- Cloud / Platform.sh
